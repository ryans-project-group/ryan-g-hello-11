terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  backend "http" {
  }
}

provider "aws" {
  region = "us-west-1"
}

resource "aws_s3_bucket" "ryan-tf-bucket" {
  bucket = "ryan-tf-bucket"
  force_destroy = true
  tags = {
    "Name" = "ryan-bucket"
  }
}

resource "aws_s3_bucket_public_access_block" "bucket-public-access-block" {
  bucket = aws_s3_bucket.ryan-tf-bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.ryan-tf-bucket.id
  key    = "g-hello-11-0.0.1-SNAPSHOT.jar"
  source = "build/libs/g-hello-11-0.0.1-SNAPSHOT.jar"
}

resource "aws_vpc" "ryan-main" {
  cidr_block       = "10.0.0.0/16"
  #instance_tenancy = "default"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "ryan-main"
  }
}

resource "aws_subnet" "ryan-public-subnet" {
  vpc_id     = aws_vpc.ryan-main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "public"
  }
}

resource "aws_subnet" "ryan-private-subnet" {
  vpc_id     = aws_vpc.ryan-main.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "private"
  }
}

resource "aws_internet_gateway" "ryan-gateway" {
  vpc_id = aws_vpc.ryan-main.id

  tags = {
  name = "ryan gateway"
  }
}

resource "aws_route_table" "ryan_rt" {
    vpc_id = aws_vpc.ryan-main.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.ryan-gateway.id
    }

    tags = {
        name = "ryan rt"
    }
}


resource "aws_route_table_association" "public_subnet" {
    count = 1
    subnet_id = element(aws_subnet.ryan-public-subnet[*].id, count.index)
    route_table_id = aws_route_table.ryan_rt.id
}


resource "aws_security_group" "allow_http_ssh" {
  name        = "allow_http_ssh"
  description = "Allow http&ssh inbound traffic"
  vpc_id      = aws_vpc.ryan-main.id

  ingress {
    description      = "HTTP from VPC"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }

  tags = {
    Name = "allow_http_ssh"
  }
}

resource "aws_iam_role_policy" "ryan-tf-policy" {
  name = "ryan-tf-policy"
  role = aws_iam_role.ryan-tf-role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
            ],
            "Resource": [
                "arn:aws:s3:::ryan-tf-bucket/*"
,
            ]
        },
        {
            "Effect": "Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource": "*"
        }
    ]
  })
}

resource "aws_iam_role" "ryan-tf-role" {
  name = "ryan-tf-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_instance_profile" "ryan-tf-profile" {
  name = "ryan-tf-profile"
  role = aws_iam_role.ryan-tf-role.name
}

resource "aws_instance" "ryan-tf-web" {
  ami           = "ami-0925fd223898ee5ba"
  instance_type = "t2.micro"
  associate_public_ip_address = true
  subnet_id = aws_subnet.ryan-public-subnet.id
  iam_instance_profile = aws_iam_instance_profile.ryan-tf-profile.name
  vpc_security_group_ids = [
    aws_security_group.allow_http_ssh.id
  ]
  key_name = "ryan-ai-prison"
  user_data = "${file("userscript.sh")}"
  tags = {
    Name = "ryan-ghello-web"
  }
}
